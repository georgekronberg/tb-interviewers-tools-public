import requests
import http.cookiejar
import pdb
import re
from datetime import datetime
from colorama import init, Fore, Back, Style
from termcolor import colored
from dateutil import parser
from bs4 import BeautifulSoup
from terminaltables import AsciiTable, DoubleTable, SingleTable, PorcelainTable, GithubFlavoredMarkdownTable
import schedule
import getch
import json
import os
import sys  

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

class TripleByte:

    def __init__(self, fileName:'file name of your schedule json'=None):

        self.scheduleFilename = fileName
        self.currentSchedule = None
        self.session = None

        self._printLogo()

        #colorama init
        init()

        try:
            with open("config.json") as json_file:
                self.config = json.load(json_file)
        except Exception as e:
            print(str(e))
            exit("Something is wrong with config.json")

        if not self.authenticateAndGetSpots():
            exit("Failed to authenticate")

        pass

    def _getSession(self):
        return self.session or requests.Session()

    def _setSession(self, session):
        self.session = session

    def _printLogo(self):
        f = open('logo.txt', 'r', encoding="utf8")
        file_contents = f.read()
        width = os.get_terminal_size().columns
        print(file_contents)
        f.close()
        print()

    def save_cookies_lwp(self, cookiejar, filename):
        lwp_cookiejar = http.cookiejar.LWPCookieJar()
        for c in cookiejar:
            args = dict(vars(c).items())
            args['rest'] = args['_rest']
            del args['_rest']
            c = http.cookiejar.Cookie(**args)
            lwp_cookiejar.set_cookie(c)
        lwp_cookiejar.save(filename, ignore_discard=True)

    def load_cookies_from_lwp(self, filename):
        lwp_cookiejar = http.cookiejar.LWPCookieJar()
        success = True
        try:
            lwp_cookiejar.load(filename, ignore_discard=True)
        except:
            success = False
        return lwp_cookiejar, success

    def mapRawToOutput(self, data):
        cleanedupDate = re.search("[0-9/]*[\s]+@[\s]+[0-9:]*[\s]+[a|p]m[\s]+[A-Z]{3}", data[3])[0].replace("@", "")
        dt = parser.parse(cleanedupDate)

        status = data[2]
        if "Confirmed" in status:
            status = Fore.MAGENTA + "*"
        elif status == "Booked and pending confirmation from candidate":
            status = "+"
        elif "Cancelled" in status:
            status = Fore.RED + "X"
        else:
            status = Style.DIM + "-"

        status = status
        
        return [data[0], status + Fore.RESET + Style.RESET_ALL, dt]

    def mapRawRequestsToOutput(self, data):
        cleanedupDate = re.search("[0-9/]*[\s]+@[\s]+[0-9:]*[\s]+[a|p]m[\s]+[A-Z]{3}", data[0])[0].replace("@", "")
        dt = parser.parse(cleanedupDate)
        return ["-", "?", dt]

    def showOnlyUpcoming(self, data):
        return data[2] > datetime.now()

    def formatDateTime(self, data):
        dateTime = "{}, in {}".format(data[2], self.formatTimeDelta(data[2]-datetime.now()))
        if data[2].date() == datetime.now().date():
            data[0] = Fore.GREEN + data[0] + Fore.RESET
        return [data[0], data[1], dateTime]

    def formatTimeDelta(sef, tdelta):
        d = {"days": tdelta.days}
        d["hours"], rem = divmod(tdelta.seconds, 3600)
        d["minutes"], d["seconds"] = divmod(rem, 60)
        total_hours, rem = divmod(tdelta.total_seconds(), 3600)
        if total_hours > 23:
            return "{days} days {hours} hours".format(**d)
        elif total_hours > 0:
            return "{hours} hours".format(**d)
        else:
            return "{minutes} minutes {seconds} seconds".format(**d)


    def getLastBookedOrPendingDate(self):
        allDates = [spot[2] for spot in self.currentSchedule]
        return max(allDates).date()

    def authenticate(self, type, login, password):
        print("Authenticate")
        print(type, login, password)
        if type == "native":
            return self.authenticateNative(login, password)
            pass
        elif type == "linkedin":
            return self.authenticateLinkedIn(login, password)
            pass
        else:
            exit("Authentication type {type} is not supported".format(type=type))

    def authenticateNative(self, login, password):
        s = self._getSession()
        getCookies = s.get(
                "https://triplebyte.com/users/sign_in", allow_redirects=True)
        soup = BeautifulSoup(getCookies.text, 'html.parser')

        URL = 'https://triplebyte.com/users/sign_in'
        UA = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'
        s.headers.update(
            {'User-Agent': UA, 'Upgrade-Insecure-Requests': '1'})

        csrfToken = soup.find("meta", {"name": "csrf-token"})["content"]
        formData = {'utf8': '✓', 'authenticity_token': csrfToken, 'user[sso_service]': '', 'user[remember_me]': 1,
                    'user[fingerprint]': '9db104368085964b89c552e741ebd0a1', 'user[email]': login, 'user[password]': password, 'commit': 'Log in'}
        tbSignIn = s.post("https://triplebyte.com/users/sign_in",
                          data=formData, allow_redirects=True, headers=dict(Referer=URL))
        self.save_cookies_lwp(tbSignIn.cookies, "cookies.txt")
        self._setSession(s)
        return tbSignIn

    def authenticateLinkedIn(self, login, password):
        s = self._getSession()
        getCookies = s.get(
                "https://triplebyte.com/users/sign_in", allow_redirects=True)
        soup = BeautifulSoup(getCookies.text, 'html.parser')

        URL = 'https://triplebyte.com/users/sign_in'
        UA = 'Remote Interviewers Toolset v1.0'
        s.headers.update(
            {'User-Agent': UA, 'Upgrade-Insecure-Requests': '1'})

        csrfToken = soup.find("meta", {"name": "csrf-token"})["content"]
        formData = {'utf8': '✓', 'authenticity_token': csrfToken, 'user[sso_service]': 'linkedin', 'user[remember_me]': 1,
                    'user[fingerprint]': '9db104368085964b89c552e741ebd0a1', 'user[email]': '', 'user[password]': ''}
        linkedin = s.post("https://triplebyte.com/users/sign_in",
                          data=formData, allow_redirects=True, headers=dict(Referer=URL))

        soup = BeautifulSoup(linkedin.text, 'html.parser')
        oauth_token = soup.find("input", {"name": "oauth_token"})["value"]
        client_id = soup.find("input", {"name": "client_id"})["value"]
        redirect_uri = soup.find(
            "input", {"name": "redirect_uri"})["value"]
        state = soup.find("input", {"name": "state"})["value"]
        scope_id = soup.find("input", {"name": "scope_id"})["value"]
        authorized = soup.find("input", {"name": "authorized"})["value"]
        csrfToken = soup.find("input", {"name": "csrfToken"})["value"]
        sourceAlias = soup.find("input", {"name": "sourceAlias"})["value"]
        formData = {'isJsEnabled': 'true', 'session_key': login, 'session_password': password, 'authorize': 'Allow access', 'oauth_token': oauth_token,
                    'client_id': client_id, 'redirect_uri': redirect_uri, 'state': state, 'scope_id': scope_id, 'authorized': authorized, 'csrfToken': csrfToken, 'sourceAlias': sourceAlias}
        tbSignIn = s.post("https://www.linkedin.com/uas/oauth2/authorizedialog/submit",
                          data=formData, allow_redirects=True)
        self.save_cookies_lwp(tbSignIn.cookies, "cookies.txt")
        self._setSession(s)
        return tbSignIn

    def authenticateAndGetSpots(self):
        authType = self.config["auth_type"]
        login = self.config[authType]["login"]
        password = self.config[authType]["password"]

        cookies, success = self.load_cookies_from_lwp('cookies.txt')
        if not success:
            tbSignIn = self.authenticate(authType, login, password)
        else:
            s = self._getSession()
            s.cookies.update(cookies)
            tbSignIn = s.get("https://www.triplebyte.com/interviewers", allow_redirects=True)
            self._setSession(s)

        if tbSignIn.status_code >= 400:
            tbSignIn = self.authenticate(authType, login, password)

        if tbSignIn != None and tbSignIn.status_code < 400:
            self.body = BeautifulSoup(tbSignIn.text, 'html.parser')
            self.parseCurrentSchedule()
            return True
        else:
            return False
        
    def parseCurrentSchedule(self):
        print("\n\nGetting current schedule...")

        soup = self.body
        table = soup.find('table', attrs={'class': 'phone-spots'})
        table_body = table.find('tbody')
        rows = table_body.find_all('tr')

        data = []

        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            actionLink = row.find('a', href=True)
            if actionLink != None:
                cols[4] = actionLink["href"]
            data.append([ele for ele in cols])

        self.currentSchedule = list(map(self.mapRawToOutput, data))

        table = soup.find('table', attrs={'class': 'requests'})
        if table != None:
            table_body = table.find('tbody')
            rows = table_body.find_all('tr')
            requestsData = []
            for row in rows:
                cols = row.find_all('td')
                cols = [ele.text.strip() for ele in cols]
                actionLink = row.find('a', href=True)
                if actionLink != None:
                    cols[1] = actionLink["href"]
                requestsData.append([ele for ele in cols])
            self.currentSchedule += list(map(self.mapRawRequestsToOutput, requestsData))

        return

    def printCurrentSchedule(self):
        print("Current schedule:")

        data = self.currentSchedule
        tableData = list(filter(self.showOnlyUpcoming, data))
        tableData = list(map(self.formatDateTime, tableData))

        table_instance = PorcelainTable(tableData)
        table_instance.inner_heading_row_border = False
        print(table_instance.table)
        pass

    def bookNewSlots(self):
        print("\n\nBooking new slots...")
        s = schedule.Schedule(self.scheduleFilename)
        newSchedule = s.generateWeekAlignedSchedule(self.getLastBookedOrPendingDate())
        total_slots = len(newSchedule)
        print("You are about to book these {} slots:".format(total_slots))
        for slot in newSchedule:
            print("{} @ {}".format(slot["start_at"], slot["start_time"]))

        print("Confirm (Y for Yes, anything else for No): ", end="")

        ch = getch.getChar().decode('UTF-8')
        print(ch)
        if ch != 'Y' and ch != 'y':
            return False

        soup = self.body
        csrfToken = soup.find("meta", {"name": "csrf-token"})["content"]

        headers = {"Referer": "https://triplebyte.com/interviewers",
                   "X-CSRF-Token": csrfToken, "X-Requested-With": "XMLHttpRequest"}

        formData = {'utf8': '✓', "phone_spot_request[start_at]": "",
                    "phone_spot_request[start_time]": "", "commit": "Submit"}

        session = self._getSession()
        for index, date in enumerate(newSchedule):
            formData["phone_spot_request[start_at]"] = date["start_at"]
            formData["phone_spot_request[start_time]"] = date["start_time"]
            print("Creating slot {} of {}...".format(index, total_slots), end="")
            createSlot = session.post("https://triplebyte.com/interviewers/phone_spot_requests",
                                data=formData, allow_redirects=True, headers=headers)
            print("Done!")

        self._setSession(session)
        return True