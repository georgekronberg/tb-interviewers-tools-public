from triplebyte import TripleByte

if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1 or len(sys.argv) > 3:
        data = dict(fileName=sys.argv[0])
        print(
            "Usage:\t{fileName} [book|show] [schedule.json]".format(**data))
    elif (len(sys.argv) == 2 or len(sys.argv) == 3) and sys.argv[1] == "book":
        fileName = None
        if len(sys.argv) == 3:
            fileName = sys.argv[2]
        tb = TripleByte(fileName)
        tb.printCurrentSchedule()
        if tb.bookNewSlots():
            tb.authenticateAndGetSpots()
            tb.printCurrentSchedule()
    elif len(sys.argv) == 2 and sys.argv[1] == "show":
        tb = TripleByte()
        tb.printCurrentSchedule()
