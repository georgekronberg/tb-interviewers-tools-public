# README #

This python script can do 2 things now:
* See your current interview schedule
* Book new interview slots

### How do I get set up? ###

* Install requirements (brom here on I assume that both pip and python are v3)

        :::javascript 
        pip install -r requirements.txt
    
    Currently there's a dependency on `NumPy`. That's a result of a stackoverflow-driven-development with close-to-zero of previous python experience of the author. Sorry. Submit a PR that removes that dependency and I will gladly accept it.

* Copy `_config.json` into `config.json`
* Edit `config.json` to have your actual credentials in it. Also change the value of the `auth_type` key in the json. Currently only `native` and `linkedin` are supported. Again, PRs are welcome, if you want to add github/facebook support.
* At this point you should be able to run this:

        :::javascript 
        python book.py show
    
    
    Which should display your current schedule. Please let me know if it doesn't!
    
* Copy `_schedule.json` into `schedule.json`
* Edit `schedule.json` to reflect your desired schedule for the next week.
* I'm hoping that the "schema" of the `schedule.json` is pretty self-explanatory, but here's the gist of it, just in case:
    * `day_schedule_presets` is an object, where the keys are arbitrary "named day schedules" and the values are arrays of timestamps, that would represent that "named day schedule"
        For example, here's my actual `day_schedule_presets`:
        
            :::javascript
            "day_schedule_presets": {
            		"5interviews": ["07:20 am", "09:50 am", "12:50 pm", "03:20 pm", "08:00 pm"],
            		"4interviews": ["07:20 am", "09:50 am", "12:50 pm", "03:20 pm"],
            		"weekend": ["02:30 pm"]
            }
            
    * `week_schedule_presets` is an object where the keys are arbitrary "named week schedules", where the values are objects as well. Keys for those objects are weekday names (you must use these strings as keys: `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`), and the values are either one of the keys from the `day_schedule_presets` or just an array of timestamps directly.
        Here's an example of how this might look like:
        
            :::javascript
            "week_schedule_presets": {
        		"full-week": {
    	    		"mon": "5interviews",
        			"tue": "5interviews",
    	    		"wed": "5interviews",
    		    	"thu": "4interviews",
    		    	"fri": "4interviews",
    		    	"sat": "weekend",
        			"sun": "weekend"
    	    	},
        		"slack-week": {
        			"wed": "weekend"
        		}
        	}
    
    * `default_week_preset` must be one of the keys from the `week_schedule_presets`
        E.g.:
        
            :::javascript
            "default_week_preset": "slack-week"
        
    * `first_day_of_week` key must have one of the weekday names as its value. This configuration bit allow you to match the scrip behavior to your mental week model. Is the first day of your week is Sunday? Set this key value to `sun. Otherwise keep its default value of `mon`. If it's not `sun` or `mon`, you're quite an unsusual fellow (but the script will still work with any day of the week set as its first day).
    
* Now, this is how all this works together:

    * The script finds the last current scheduled interview, and looks for the next closest `first_day_of_week`.
        E.g., if you last currently scheduled interview is on August 1st, 2017 (Tuesday), and your `first_day_of_week` is set to `mon`, the script will only book interviews for this date range: August 7th 2017 (Monday) - August 13th 2017 (Sunday).
    * After that, based on the values of `default_week_preset`, `day_schedule_presets` and `week_schedule_presets` the script will assemble your effective active schedule for the upcoming week. Given the examples given above, the effective schedule will look like this:
        
            :::javascript    
            {
                "wed": ["02:30 pm"]
            }
        
    * After that, the script will map this schedule of the previosly defined date range, and will ask for your confirmation:
          
            :::javascript 
            Booking new slots...
            You are about to book these 1 slots:
            2017-08-09 @ 02:30 pm
            Confirm (Y for Yes, anything else for No):
    
        
        If you hit `Y` it will go ahead and book that slot.
        If you hit anything else other than `Y` it will exit.
        
    * Boom!