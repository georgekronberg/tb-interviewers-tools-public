from datetime import datetime
from datetime import date
from datetime import timedelta
from colorama import init, Fore, Back, Style
from termcolor import colored
from dateutil import parser
import numpy as np
import json
import calendar

class Schedule:

    def __init__(self, fileName:'file name of your schedule json'=None):
        self.fileName = "schedule.json"
        if fileName is not None:
            self.fileName = fileName

        #colorama init
        init()
        try:
            with open(self.fileName) as json_file:
                self.schedule = json.load(json_file)
                #print(self.schedule)
        except:
            exit("Something is wrong with {fileName}".format(fileName=self.fileName))
        self.parseSchedule()
        pass

    def mapDaySchedules(self, weekSchedule):
        if isinstance(weekSchedule, str):
            weekSchedule = self.day_schedule_presets[weekSchedule]
        return weekSchedule

    def parseSchedule(self):
        expected_attributes = ["first_day_of_week", "day_schedule_presets", "week_schedule_presets", "default_week_preset"]
        missing_list = np.setdiff1d(expected_attributes, list(self.schedule.keys()))
        if missing_list.size:
            exit('Malformed {fileName}, missing attributes: {missing_list}'.format(fileName=self.fileName, missing_list=missing_list))  
        pass
        self.day_schedule_presets = self.schedule["day_schedule_presets"]
        self.first_day_of_week = self.schedule["first_day_of_week"]
        day_schedule_presets = self.schedule["day_schedule_presets"].keys()
        week_schedule_presets = self.schedule["week_schedule_presets"].keys()
        default_week_preset = self.schedule["default_week_preset"]
        if not isinstance(default_week_preset, str):
            exit('Malformed {fileName}, {default_week_preset} is not a string'.format(fileName=self.fileName, default_week_preset=default_week_preset))  
        current_week = self.schedule["week_schedule_presets"][default_week_preset]
        expected_week_days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
        unknown_days = np.setdiff1d(list(current_week.keys()), expected_week_days)
        if unknown_days.size:
            exit('Malformed {fileName}, unknown week days: {unknown_days}'.format(fileName=self.fileName, unknown_days=unknown_days))  
        #print(type(current_week))
        #print(list(current_week.items()))
        #self.current_schedule = current_week 

        self.current_schedule = {k:self.mapDaySchedules(v) for k,v in current_week.items()}

        # for k, v in current_schedule.iteritems():
        #     d[k] = self.mapDaySchedules(v)
        pass

    def getDayNumber(self, dayName):
        return next(i for i,v in enumerate(list(calendar.day_abbr)) if v.lower() == dayName.lower())

    def generateWeekAlignedSchedule(self, currentLastBookedDate):        
        first_day_of_week_number = self.getDayNumber(self.first_day_of_week)
        onDay = lambda date, day: date + timedelta(days=(day-date.weekday()+7)%7)

        #print(self.current_schedule)
        first_day_of_blank_week = onDay(currentLastBookedDate + timedelta(days=1), first_day_of_week_number)
        #print(first_day_of_blank_week)
        finalSchedule = [{"start_at":onDay(first_day_of_blank_week, self.getDayNumber(k)).strftime('%Y-%m-%d'),"start_time":timeSlot} for k,v in self.current_schedule.items() for timeSlot in v]
        return sorted(finalSchedule, key=lambda d: d['start_at'], reverse=False)
        

        


# s = Schedule()
# s.renderSchedule()
# s.generateWeekAlignedSchedule(date(2017, 7, 5))

